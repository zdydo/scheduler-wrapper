import React, { Component } from 'react';
import {
    Scheduler,
    Views,
    Day,
    Month,
    Lightbox,
    Textarea,
    TimeDate,
    Select,
    Multiselect,
    RecurringSection,
    Markers,
    TodayLine,
    CursorPosition,
    Events
} from './components/Scheduler';
import './App.css';

class App extends Component {
    render() {
        return (
            <Scheduler
                showLoading={true}
                dblclickCreate={true}
            >
                <Events
                    dataPath='data/events.json'
                />
                <Views
                    startView='month'
                >
                    <Month
                        maxMonthEvents={5}
                    />
                    <Day/>
                </Views>
                <Lightbox>
                    <Textarea
                        label='Name'
                        height={45}
                        focus={true}
                        defaultValue='Event name'
                        mapToField='name'
                    />
                    <Multiselect
                        name='staff'
                        label='Staff'
                        height={50}
                        mapToField='staff'
                        options='staff'
                        vertical={true}
                    />
                    <Select
                        name='room'
                        label='Room'
                        height={40}
                        mapToField='room'
                        defaultValue={1}
                        options='rooms'
                    />
                    <RecurringSection
                        label='Repeat event'
                        button={true}
                    />
                    <TimeDate
                        label='Time'
                        name='time'
                        height={50}
                        timePicker={true}
                        timeStep={30}
                    />
                </Lightbox>
                <Markers>
                    <TodayLine/>
                    <CursorPosition
                        views={['month']}
                        css='highlighted_month_day'
                    />
                </Markers>
            </Scheduler>
        );
    }
}
export default App;