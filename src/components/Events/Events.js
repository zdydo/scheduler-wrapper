import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Events extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}