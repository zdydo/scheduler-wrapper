import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Header extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
    }

    setTabName(view, name) {
        if (name) {
            this.scheduler.locale.labels[`${view}_tab`] = name;
        }
    }

    initTabs() {
        const {
            dayTab,
            weekTab,
            monthTab,
            yearTab,
            agendaTab,
            mapTab
        } = this.props;

        this.setTabName('day', dayTab);
        this.setTabName('week', weekTab);
        this.setTabName('month', monthTab);
        this.setTabName('year', yearTab);
        this.setTabName('agenda', agendaTab);
        this.setTabName('map', mapTab);
    }

    initEvents() {
        const { onBeforeTodayDisplayed } = this.props;

        if (onBeforeTodayDisplayed) {
            this.scheduler.attachEvent('onBeforeTodayDisplayed', onBeforeTodayDisplayed);
        }
    }

    componentDidMount() {
        this.initTabs();
        this.initEvents();
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}