
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_editors';

export default class Checkbox extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            mapToField,
            defaultValue,
            checkedValue,
            uncheckedValue,
            addControl
        } = this.props;

        let config = { type: 'checkbox' };
        config.name = name || 'checkbox';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.map_to = mapToField;

        if (defaultValue) {
            config.default_value = defaultValue;
        }
        if (checkedValue) {
            config.checked_value = checkedValue;
        }
        if (uncheckedValue) {
            config.unchecked_value = uncheckedValue;
        }

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}