import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_readonly';

export default class Lightbox extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;

        this.addControl = this.addControl.bind(this);

        this.state = {
            controls: []
        };
    }

    addControl(control) {
        this.setState({
            controls: this.state.controls.push(control)
        });
    }

    initEvents() {
        if (this.scheduler._$initialized) {
            return;
        }
        const {
            onAfterLightbox,
            onBeforeLightbox,
            onLightbox,
            onLightboxButton
        } = this.props;

        this.helper.setHandler('onAfterLightbox', onAfterLightbox);
        this.helper.setHandler('onBeforeLightbox', onBeforeLightbox);
        this.helper.setHandler('onLightbox', onLightbox);
        this.helper.setHandler('onLightboxButton', onLightboxButton);
    }

    componentDidMount() {
        const {
            readOnly,
            createByLightbox,
            openOnDblclick,
            dragByHeader,
            responsive
        } = this.props;

        this.scheduler.config.readonly_form = readOnly || false;
        if (createByLightbox) {
            this.scheduler.config.details_on_create = createByLightbox;
            this.scheduler.config.edit_on_create = createByLightbox;
        }
        this.scheduler.config.details_on_dblclick = openOnDblclick || false;
        if (dragByHeader !== undefined) {
            this.scheduler.config.drag_lightbox = dragByHeader;
        }
        this.scheduler.config.responsive_lightbox = responsive || false;

        this.scheduler.config.lightbox.sections = this.state.controls;

        this.initEvents();
    }

    render() {
        const childrenWithProps = React.Children.map(this.props.children, child => React.cloneElement(child, { addControl: this.addControl }));
        return (
            <div>
                {childrenWithProps}
            </div>
        );
    }
}