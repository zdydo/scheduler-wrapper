
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_multiselect';

export default class Multiselect extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            mapToField,
            defaultValue,
            options,
            vertical,
            delimiter,
            addControl
        } = this.props;

        let config = { type: 'multiselect' };
        config.name = name || 'multiselect';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.map_to = mapToField;
        config.options = [];
        if (options) {
            if (Array.isArray(options)) {
                config.options = options;
            } else {
                config.options = this.scheduler.serverList(options);
            }
        }

        config.vertical = vertical || false;

        if (defaultValue && !this.scheduler._$initialized) {
            this.scheduler.attachEvent('onEventCreated', (id, e) => {
                this.scheduler.getEvent(id)[mapToField] = defaultValue;
                this.scheduler.updateEvent(id);
                return true;
            });

            config.default_value = defaultValue;
        }

        if (delimiter) {
            config.delimiter = delimiter;
        }

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}