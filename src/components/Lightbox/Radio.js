
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Radio extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            mapToField,
            defaultValue,
            options,
            vertical,
            addControl
        } = this.props;

        let config = { type: 'radio' };
        config.name = name || 'radio';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.map_to = mapToField;

        if (defaultValue) {
            config.default_value = defaultValue;
        }

        config.options = options || [];
        config.vertical = vertical || false;

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}