
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_recurring';

export default class RecurringSection extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            mapToField,
            button,
            addControl
        } = this.props;

        let config = { type: 'recurring' };
        config.name = name || 'recurring';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.map_to = mapToField || 'rec_type';

        if (button) {
            config.button = 'recurring';
        }

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}