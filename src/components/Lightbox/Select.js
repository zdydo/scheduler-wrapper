
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Select extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            focus,
            mapToField,
            defaultValue,
            options,
            onchange,
            addControl
        } = this.props;

        let config = { type:'select' };
        config.name = name || 'select';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }

        if (height) {
            config.height = height;
        }

        config.focus = focus || false;
        config.map_to = mapToField;
        if (defaultValue) {
            config.default_value = defaultValue;
        }
        config.options = [];
        if (options) {
            if (Array.isArray(options)) {
                config.options = options;
            } else {
                config.options = this.scheduler.serverList(options);
            }
        }
        if (onchange) {
            config.onchange = onchange;
        }

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}