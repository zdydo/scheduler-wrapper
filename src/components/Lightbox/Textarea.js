
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Textarea extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            name,
            label,
            height,
            focus,
            defaultValue,
            mapToField,
            addControl
        } = this.props;

        let config = { type:'textarea' };
        config.name = name || 'text';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.focus = focus || false;

        if (defaultValue && !this.scheduler._$initialized) {
            this.scheduler.attachEvent('onEventCreated', (id, e) => {
                this.scheduler.getEvent(id)[mapToField] = defaultValue;
                this.scheduler.updateEvent(id);
                return true;
            });

            config.default_value = defaultValue;
        }

        config.map_to = mapToField || 'text';
        
        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}