
import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class TimeDate extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            label,
            name,
            height,
            focus,
            timePicker,
            yearRange,
            format,
            limitTime,
            autoEnd,
            eventDuration,
            timeStep,
            addControl
        } = this.props;

        let config = { type:'time', map_to: 'auto' };
        config.name = name || 'time';
        if (label) {
            this.scheduler.locale.labels[`section_${config.name}`] = label;
        }
        
        if (height) {
            config.height = height;
        }

        config.focus = focus || false;

        if (timePicker) {
            config.type = 'calendar_time';
        }

        if (yearRange) {
            config.year_range = yearRange;
        }

        if (format) {
            if (timePicker) {
                this.scheduler.templates.time_picker = Array.isArray(format)
                    ? this.scheduler.date.date_to_str(format[0])
                    : this.scheduler.date.date_to_str(format);
            } else {
                config.time_format = format;
            }
        }

        if (timeStep) {
            this.scheduler.config.time_step = timeStep;
        }
        this.scheduler.config.limit_time_select = limitTime || false;
        this.scheduler.config.auto_end_date = autoEnd || false;
        if (eventDuration) {
            this.scheduler.config.event_duration = eventDuration;
        }

        addControl(config);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}