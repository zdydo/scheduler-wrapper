import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';


export default class BlockedTime extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            children,
            className,
            startDate,
            endDate,
            days,
            zones,
            invertZones

        } = this.props;

        let config = { type: 'dhx_time_block' };

        if (startDate) {
            config.start_date = new Date(startDate);
        }
        if (endDate) {
            config.end_date = new Date(endDate);
        }
        if (days) {
            config.days = days;
        }
        config.zones = zones || 'fullday';
        config.invert_zones = invertZones || false;
        if (className) {
            config.css = className;
        }

        let childrenString = '';
        React.Children.forEach(children, child => {
            childrenString += this.helper.convertJSXToString(child);
        });
        config.html = childrenString;

        this.scheduler.addMarkedTimespan(config);
    }

    render() {
        return (
            null
        );
    }
}