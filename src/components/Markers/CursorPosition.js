import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class CursorPosition extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;

        this.state = {
            prevElement: null,
            currentDate: null
        };
    }

    clearPrevClass(css) {
        if (this.state.prevElement) {
            this.state.prevElement.classList.toggle(css, false);
        }
    }

    highlightDate(eventId, e, views, css) {
        if (views && !views.find(viewName => viewName == this.scheduler.getState().mode)) {
            return;
        } 

        const date = this.scheduler.getActionData(e).date;

        if (eventId) {
            this.clearPrevClass(css);
            this.setState({
                prevElement: null,
                currentDate: null
            });
            return;
        }
        if (date && (!this.state.currentDate || +date != +this.state.currentDate)) {
            let el = e.target || e.srcElement;

            while (el && el.tagName != "TD") {
                el = el.parentNode;
            }
            if (el && el != this.state.prevElement) {
                this.clearPrevClass(css);

                el.classList.toggle(css, true);
                this.setState({
                    prevElement: el,
                    currentDate: date
                });
            } else {
                if (el != this.state.prevElement) {
                    this.clearPrevClass(css);
                    this.setState({
                        prevElement: null,
                        currentDate: null
                    });
                }
            }
        }
    }

    componentDidMount() {
        const {
            views,
            css
        } = this.props;
        
        this.scheduler.attachEvent('onMouseMove', (eventId, e) => {
            debugger
            this.highlightDate(eventId, e, views, css);
        });

    }

    render() {
        return (
            null
        );
    }
}