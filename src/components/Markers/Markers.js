import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_limit';

export default class Markers extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;

        this.addTodayLine = this.addTodayLine.bind(this);

        this.scheduler.config.mark_now = false;
    }

    addTodayLine() {
        this.scheduler.config.mark_now = true;
    }

    render() {
        const childrenWithProps = React.Children.map(this.props.children, child => {
            if (child.type.name.toLowerCase() === 'todayline') {
                return React.cloneElement(child, { addTodayLine: this.addTodayLine });
            }
            return React.cloneElement(child);
        });
        return (
            <div>
                {childrenWithProps}
            </div>
        );
    }
}