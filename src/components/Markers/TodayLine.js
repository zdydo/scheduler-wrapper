import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class TodayLine extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            nowDate,
            addTodayLine
        } = this.props;

        if (nowDate) {
            this.scheduler.config.now_date = new Date(nowDate);
        }

        addTodayLine(true);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}