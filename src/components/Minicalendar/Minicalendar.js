
import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_minical';

export default class Minicalendar extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            formatDate,
            headerDateFormat,
            scaleDateFormat,
            formatSelectDate
        } = this.props;

        if (formatDate) {
            this.helper.setDateFormat(formatDate, null, 'calendar_date');
        }
        if (headerDateFormat) {
            this.helper.setDateFormat(headerDateFormat, null, 'calendar_month');
        }
        if (scaleDateFormat) {
            this.helper.setDateFormat(scaleDateFormat, null, 'calendar_scale_date');
        }
        if (formatSelectDate) {
            this.helper.setDateFormat(formatSelectDate, null, 'calendar_time');
        }

    }

    render() {
        return (
            null
        );
    }
}