import React, { Component } from 'react';
import 'dhtmlx-scheduler';
import 'dhtmlx-scheduler/codebase/dhtmlxscheduler_material.css';
import Helper from '../helper';
import './'

const scheduler = window.scheduler;
export const SchedulerContext = React.createContext();

export default class Scheduler extends Component {

    constructor(props) {
        super(props);
        this.helper = new Helper(scheduler);

        scheduler.config.mark_now = false;
    }

    getChild (children, componentName) {
        if (!children) {
            return;
        }
        let child;
        React.Children.forEach(children, el => {
            if (el.type.name.toLowerCase() === componentName.toLowerCase()) {
                child = el;
            }
        });
        return child;
    }

    headerInit (views, headerComponent) {
        let header = [];
        if (headerComponent) {
            const {
                date,
                prevBtn,
                nextBtn,
                todayBtn
            } = headerComponent.props;

            if (date || date === undefined) {
                header.push('date');
            }

            if (prevBtn || prevBtn === undefined) {
                header.push('prev');
            }

            if (todayBtn || todayBtn === undefined) {
                header.push('today');
            }

            if (nextBtn || nextBtn === undefined) {
                header.push('next');
            }

        } else {
            header = [
                'date',
                'prev',
                'today',
                'next'
            ]
        }
        scheduler.config.header = views.concat(header);
    }

    initEvents() {
        if (scheduler._$initialized) {
            return;
        }

        const {
            onAfterSchedulerResize,
            onSchedulerResize,
            onBeforeParse,
            onDataRender,
            onLoadEnd,
            onLoadError,
            onLoadStart,
            onParse,
            onSchedulerReady,
            onTemplatesReady
        } = this.props;

        this.helper.setHandler('onAfterSchedulerResize', onAfterSchedulerResize);
        this.helper.setHandler('onSchedulerResize', onSchedulerResize);
        this.helper.setHandler('onBeforeParse', onBeforeParse);
        this.helper.setHandler('onDataRender', onDataRender);
        this.helper.setHandler('onLoadEnd', onLoadEnd);
        this.helper.setHandler('onLoadError', onLoadError);
        this.helper.setHandler('onLoadStart', onLoadStart);
        this.helper.setHandler('onParse', onParse);
        this.helper.setHandler('onSchedulerReady', onSchedulerReady);
        this.helper.setHandler('onTemplatesReady', onTemplatesReady);
        scheduler._$initialized = true;
    }

    componentDidMount() {
        const {
            children,
            rtl,
            showLoading,
            dblclickCreate
        } = this.props;

        scheduler.skin = 'material';
        scheduler.config.rtl = rtl || false;
        scheduler.config.show_loading = showLoading || false;
        if (dblclickCreate !== undefined) {
            scheduler.config.dblclick_create = dblclickCreate;
        }

        this.initEvents();
        
        const viewsComponent = this.getChild(children, 'views');
        const viewsList = this.helper.getViews(viewsComponent);

        const headerComponent = this.getChild(children, 'header');
        this.headerInit(viewsList, headerComponent);
        
        if (!this.getChild(children, 'lightbox')) {
            scheduler.config.lightbox.sections = [];
        }

        const startDate = viewsComponent && viewsComponent.props.startDate ? new Date(viewsComponent.props.startDate) : new Date();
        const startView = viewsComponent && viewsComponent.props.startView ? viewsComponent.props.startView : 'week';

        scheduler.init(this.schedulerContainer, startDate, startView);
        scheduler.clearAll();
        const eventsComponent = this.getChild(children, 'events');
        if (eventsComponent) {
            const {
                data,
                dataPath
            } = eventsComponent.props;
            if (data) {
                scheduler.parse(data);
            } else if (dataPath) {
                scheduler.load(dataPath);
            }
        }
    }

    render() {
        const contextObj = { scheduler, helper: this.helper };

        return (
            <div
                ref={ (input) => { this.schedulerContainer = input } }
                style={ { width: '100vw', height: '100vh' } }
            >
                <SchedulerContext.Provider value={contextObj}>
                    {this.props.children}
                </SchedulerContext.Provider>
            </div>
        );
    }
}
