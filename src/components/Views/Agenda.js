import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_agenda_view';

export default class Agenda extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props, context);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            startDate,
            endDate,
            enableNavBtns,
            headerDateFormat,
            timeFormat
        } = this.props;

        if (startDate) {
            this.scheduler.config.agenda_start = new Date(startDate); 
        }
        if (endDate) {
            this.scheduler.config.agenda_end = new Date(endDate);
        }

        if (enableNavBtns) {
            this.scheduler.date.agenda_start = date => this.scheduler.date.month_start(new Date(date)); 
            this.scheduler.date.add_agenda = (date, inc) => this.scheduler.date.add(date, inc, 'month'); 
        }

        this.helper.setDateFormat(headerDateFormat, 'agenda');
        if (timeFormat) {
            const formatFunc = this.scheduler.date.date_to_str(timeFormat);
            this.scheduler.templates.agenda_time = (start, end, ev) => {
                if (this.scheduler.isOneDayEvent(ev)) {
                    return formatFunc(start);
                }
                    
                return `${formatFunc(start)} - ${formatFunc(end)}`;
            };
        }
    }

    render() {
        return (
            null
        );
    }
}