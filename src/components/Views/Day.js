import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Day extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props, context);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            headerDateFormat,
            scaleDateFormat
        } = this.props;

        this.helper.setDateFormat(headerDateFormat, 'day');
        this.helper.setDateFormat(scaleDateFormat, 'day', 'day_scale_date');
    }

    render() {
        return (
            null
        );
    }
}