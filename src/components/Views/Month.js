import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Month extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props, context);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            headerDateFormat,
            scaleDateFormat,
            dayFormat,
            maxMonthEvents,
            skipDays,
            resizeEvents,
            onViewMoreClick
        } = this.props;

        this.helper.setDateFormat(headerDateFormat, 'month');
        this.helper.setDateFormat(scaleDateFormat, 'month', 'month_scale_date');
        if (dayFormat) {
            this.scheduler.config.month_day = dayFormat;
        }

        this.helper.skipDays(skipDays, 'month');

        if (maxMonthEvents) {
            this.scheduler.config.max_month_events = maxMonthEvents;
        }

        this.scheduler.config.resize_month_events = resizeEvents || false;
        this.scheduler.config.resize_month_timed = resizeEvents || false; 

        if (!this.scheduler._$initialized) {
            this.helper.setHandler('onViewMoreClick', onViewMoreClick);
        }
    }

    render() {
        return (
            null
        );
    }
}