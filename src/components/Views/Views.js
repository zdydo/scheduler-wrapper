import React, { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_limit';

export default class Views extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    initEvents() {
        if (this.scheduler._$initialized) {
            return;
        }

        const {
            onBeforeViewChange,
            onViewChange,
            onScaleAdd
        } = this.props;

        this.helper.setHandler('onBeforeViewChange', onBeforeViewChange);
        this.helper.setHandler('onViewChange', onViewChange);
        this.helper.setHandler('onScaleAdd', onScaleAdd);
    }

    componentDidMount() {
        const {
            firstHour,
            lastHour,
            hourScale,
            hourSize,
            readonly,
            startOnMonday,
            defaultDate,
            scrollHour,
            skipDays,
            limitRange
        } = this.props;

        if (firstHour) {
            this.scheduler.config.first_hour = firstHour;
        }

        if (lastHour) {
            this.scheduler.config.last_hour = lastHour;
        }

        if (hourScale) {
            this.scheduler.config.hour_date = hourScale;
        }

        if (hourSize) {
            this.scheduler.config.hour_size_px = hourSize;
        }

        this.scheduler.config.readonly = readonly || false;

        if (startOnMonday !== undefined) {
            this.scheduler.config.start_on_monday = startOnMonday;
        }

        if (defaultDate) {
            this.scheduler.config.default_date = defaultDate;
        }

        if (scrollHour) {
            this.scheduler.config.scroll_hour = scrollHour;
        }

        if (skipDays && skipDays.length) {
            const views = this.helper.getViews(this);
            if (views && views.length) {
                views.forEach(viewName => {
                    if (viewName !== 'day' && !this.scheduler[`ignore_${viewName}`]) {
                        this.helper.skipDays(skipDays, viewName);
                    }
                });
            }
        }

        if (limitRange) {
            this.scheduler.config.limit_view = true;
            if (limitRange.start) {
                this.scheduler.config.limit_start = new Date(limitRange.start);
            }
            if (limitRange.end) {
                this.scheduler.config.limit_end = new Date(limitRange.end);
            }
        }

        this.initEvents();
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}