import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';

export default class Week extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props, context);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            skipDays,
            headerDateFormat,
            dayCellCss,
            scaleDateFormat
        } = this.props;

        this.helper.skipDays(skipDays, 'week');

        this.helper.setDateFormat(headerDateFormat, 'week');
        this.helper.setDateFormat(scaleDateFormat, 'week', 'week_scale_date');

        // need to add class for holiday cell
        if (dayCellCss) {
            if (typeof dayCellCss == 'string') {
                this.scheduler.templates.week_date_class = (start, today) => {
                    return dayCellCss;
                };
            } else {
                this.scheduler.templates.week_date_class = dayCellCss;
            }
        }
    }

    render() {
        return (
            null
        );
    }
}