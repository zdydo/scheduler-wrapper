import { PureComponent } from 'react';
import { SchedulerContext } from '../Scheduler/Scheduler';
import 'dhtmlx-scheduler/codebase/ext/dhtmlxscheduler_year_view';

export default class Year extends PureComponent {
    static contextType = SchedulerContext;

    constructor(props, context) {
        super(props, context);
        this.scheduler = context.scheduler;
        this.helper = context.helper;
    }

    componentDidMount() {
        const {
            numbMonthsInRow,
            numbMonthsInColumn,
            headerDateFormat,
            scaleDateFormat,
            monthFormat,
            dayFormat,
            skipDays
        } = this.props;

        if (numbMonthsInRow) {
            this.scheduler.config.year_x = numbMonthsInRow;
        }

        if (numbMonthsInColumn) {
            this.scheduler.config.year_y = numbMonthsInColumn;
        }

        this.helper.setDateFormat(headerDateFormat, 'year');
        this.helper.setDateFormat(scaleDateFormat, 'year', 'year_scale_date');
        this.helper.setDateFormat(monthFormat, 'year', 'year_month');
        if (dayFormat) {
            this.scheduler.config.month_day = dayFormat;
        }

        this.helper.skipDays(skipDays, 'year');

    }

    render() {
        return (
            null
        );
    }
}