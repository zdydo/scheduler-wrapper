import React from 'react';
import stringify from 'json-stringify-pretty-compact';
import indentString from 'indent-string';

export default class Helper {
    constructor(scheduler) {
        this.scheduler = scheduler;
    }

    setHandler(eventName, handler) {
        if (handler) {
            this.scheduler.attachEvent(eventName, handler);
        }
    }

    setDateFormat(format, view, type = '') {
        if (format) {
            const formatDate = this.scheduler.date.date_to_str(format);
            const template = type || `${view}_date`;
            this.scheduler.templates[template] = date => formatDate(date);
        }
    }

    getViews (viewsComponent) {
        let viewNames;
        if (viewsComponent && viewsComponent.props.children) {
            const views = viewsComponent.props.children;
            viewNames = React.Children.map(views, view => view.type.name.toLowerCase());
        }
        return viewNames;
    }

    skipDays(skipDays, view) {
        if (skipDays && skipDays.length) {
            this.scheduler[`ignore_${view}`] = date => {
                if (skipDays.indexOf(date.getDay()) >= 0) {
                    return true;
                }
            };
        }
    }

    convertJSXToString(JSXElement) {
        const name = this.getElementName(JSXElement);
        const props = this.getProps(JSXElement);
        const children = this.getChildren(JSXElement);
        const hasChildren = children.length > 0;

        let result = `<${name}${props}/>`;
        if (hasChildren) {
            result = `<${name}${props}>${children}</${name}>`;
        }

        return result;
    }

    getElementName(JSXElement) {
        const type = JSXElement.type;
        let name = 'Unknown';
        if (type.displayName) {
            name = type.displayName;
        } else if (type.name) {
            name = type.name;
        } else if (typeof type == 'string') {
            name = type;
        }

        return name;
    }

    getProps(JSXElement) {
        if (!JSXElement.props) {
            return;
        }

        let props = Object.keys(JSXElement.props).map(propName => {
            let prop = JSXElement.props[propName];
            let result = '';

            if (propName != 'children' && !(JSXElement.type.defaultProps && JSXElement.type.defaultProps[propName] === prop)) {
                if (typeof prop == 'string') {
                    result = ' ' + propName + '=' + JSON.stringify(prop);
                } else {
                    if (React.isValidElement(prop)) {
                        prop = this.convertJSXToString(prop);
                    }

                    if (typeof prop == 'object') {
                        prop = stringify(prop);
                    }
                    result = ` ${propName}={${prop}}`;

                    if (typeof prop == 'function') {
                        prop = prop.toString().replace(/"/g, `'`);
                        result = ` ${propName}="(${prop})()"`
                    }

                    
                }
            }

            return result;
        });

        return props.join('');
    }

    getChildren(JSXElement) {
        const children = JSXElement.props.children;
        if (!children) {
            return;
        }

        const resultChildren = React.Children.map(children, child => this.getChild(child));
        const content = resultChildren.filter(Boolean).join('\n');
      
        return `\n${indentString(content, 2)}\n`;
    }

    getChild(element) {
        let child = String(element);

        if (React.isValidElement(element)) {
            child = this.convertJSXToString(element);
        } else if (element == null || element === false) {
          child = '';
        }
      
        return child;
      }

}