import React, { Component } from 'react';
import {
    Scheduler,
    Header,
    Views,
    Day,
    Week,
    Month,
    Agenda,
    Year,
    Lightbox,
    Textarea,
    TimeDate,
    Minicalendar,
    Select,
    Multiselect,
    Checkbox,
    Radio,
    Markers,
    TodayLine,
    BlockedTime,
    MarkedTime,
    Events
} from './components/Scheduler';
import './App.css';

const data = [
    { start_date:'2020-06-10 6:00', end_date:'2020-06-10 8:00', text:'Event 1', id: 1 },
    { start_date:'2020-06-13 10:00', end_date:'2020-06-14 18:00', text:'Event 2', id: 2 }
];

const owners = [
    { key: 1, label: 'Johnny' },
    { key: 2, label: 'Adrian' },
    { key: 3, label: 'Luisa' }
];

const priorities = [
    { key: 1, label: 'High' },
    { key: 2, label: 'Medium' },
    { key: 3, label: 'Low' }
];

const clickHandler = () => {
    alert('block!');
};

class App extends Component {

    render() {
        return (
            <Scheduler
                rtl={false}
                showLoading={true}
                dblclickCreate={true}
            >
                <Events
                    data={data}
                />
                <Header
                    dayTab='Day'
                    weekTab='Week'
                    monthTab='Month'
                    yearTab='Year'
                    agendaTab='Agenda'
                    mapTab='Map'
                    date={true}
                    prevBtn={true}
                    todayBtn={true}
                    nextBtn={true}
                >
                </Header>
                <Views
                    // startDate={new Date(2020, 3, 10)}
                    startView='month'
                    defaultDate='%j %M'
                    hourScale=''
                    hourSize='44'
                    firstHour='3'
                    lastHour='24'
                    scrollHour='5'
                    startOnMonday={false}
                    readonly={false}
                    skipDays={[0, 6]}
                    limitRange={{start: new Date(2015, 0, 1), end: new Date(2025, 0, 1)}}
                    timeStep={30}
                >
                    <Day
                        headerDateFormat='%j %F %Y'
                        scaleDateFormat='%j %F, %l'
                    />
                    <Week
                        skipDays={[0, 6]}
                        headerDateFormat=''
                        scaleDateFormat=''
                        dayCellCss=''
                    />
                    <Month
                        headerDateFormat=''
                        scaleDateFormat=''
                        dayFormat='%j'
                        maxMonthEvents={3}
                        skipDays={[0, 6]}
                        resizeEvents={true}
                    />
                    <Agenda
                        headerDateFormat='%F %Y'
                        timeFormat='%j %M %G:%i'
                        startDate=''
                        endDate=''
                        enableNavBtns={true}
                    />
                    <Year
                        numbMonthsInRow={3}
                        numbMonthsInColumn={4}
                        headerDateFormat=''
                        scaleDateFormat=''
                        monthFormat='%M'
                        dayFormat=''
                        skipDays={[1, 2]}
                    />
                </Views>
                <Lightbox
                    readOnly={false}
                    createByLightbox={true}
                    openOnDblclick={true}
                    dragByHeader={false}
                    responsive={true}
                >
                    <Textarea
                        label='Description'
                        height={45}
                        focus={false}
                        defaultValue='description'
                        mapToField='text'
                    />
                    <TimeDate
                        label='Time period'
                        name='time'
                        height={50}
                        format={['%H-%i', '%m', '%d', '%Y']}
                        focus={false}
                        timePicker={true}
                        yearRange={[2010, 2025]}
                        limitTime={true}
                        autoEnd={true}
                        eventDuration={120}
                        timeStep={30}
                    >
                        <Minicalendar
                            formatDate='%j'
                            headerDateFormat='%M %y'
                            formatSelectDate='%Y/%n/%j'
                        />
                    </TimeDate>
                    <Select
                        name='owner'
                        label='Owner'
                        height={40}
                        focus={true}
                        mapToField='owner'
                        defaultValue={3}
                        options={owners}
                        onchange
                    />
                    <Multiselect
                        name='user'
                        label='Users'
                        height={50}
                        mapToField='users'
                        options={owners}
                        defaultValue={'2;3'}
                        vertical={true}
                        delimiter=';'
                    />
                    <Checkbox
                        name='checkbx'
                        label='Is true'
                        height={40}
                        mapToField='isTrue'
                        defaultValue={true}
                        checkedValue={true}
                        uncheckedValue={false}
                    />
                    <Radio
                        name='priority'
                        label='Priority'
                        height={58}
                        mapToField='priority'
                        defaultValue={2}
                        options={priorities}
                        vertical={false}
                    />
                </Lightbox>
                <Markers>
                    <TodayLine/>
                    <BlockedTime
                        className='blocked_time'
                        days={new Date(2020, 2, 11)}
                        zones={[9 * 60, 13 * 60, 14 * 60, 18 * 60]}
                        invertZones={true}
                    >
                        <b onClick={clickHandler}>Blocked</b>
                    </BlockedTime>
                    <MarkedTime
                        className='highlight_time'
                        days={new Date(2020, 2, 12)}
                    >
                        <b className='temp'>Highlight</b>
                    </MarkedTime>
                </Markers>
            </Scheduler>
        );
    }
}
export default App;